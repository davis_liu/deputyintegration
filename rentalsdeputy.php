<?php
require 'vendor/autoload.php';


use Debug\Debug;
use NewRelic\NewRelic;


date_default_timezone_set('Pacific/Auckland');


class RentalsDeputy {


	//API call to get current pickup and drop off
	private $thlAPIRequest_url = 'https://api.thlonline.com/api/1/totalVehicles?countryCode=';
	
	//List of countries that are using the Aurora / Deputy integration
	private $locations = array('AU');

	//deputy API Keys
	private $deputy_endpoint = 'tourismholdingsaustraliaptyl.au.deputy.com';	
	private $deputy_token = 'e908486c9192f31838b32e74b5b7a939';

	private $OperationalUnit = '';
	
	// Translate the difference between Deputy branch codes and aurora branch codes
	private $unitTranslation = array('ADL'=>'ADE',
									 'ASP' => 'ALI',
									 'BNE' => 'BRI',
									 'CNS' => 'CAI',
									 'DRW' => 'DAR'
									 );

	// Use the correct time zones for each branch
	// Currently this is not being used do to the fact that the deput API is correctly using the timestamp
	private $unitTimeZone = array('ADE' => 'Australia/Adelaide',
								  'SYD' => 'Australia/Sydney',
								  'MEL' => 'Australia/Melbourne',
								  'ALI' => 'Australia/Darwin',
								  'BRI' => 'Australia/Brisbane',
								  'CAI' => 'Australia/Brisbane',
								  'DAR' => 'Australia/Darwin',
								  'PER' => 'Australia/Perth'
								  );

	
	public function __construct() {


		Debug::debugON(TRUE);
		$newRelic = new NewRelic();
		$newRelic->createEvent(json_encode(array('eventType'=>'scriptExecute',
									 'scriptName'=>'rentalsdeputy.php',
									 'scriptStart'=>1)));

		// Get the list of operational units for each branch
		$data = $this->_deputyAPI('resource/OperationalUnit/');
		$this->OperationalUnit = $data;

		// cycle through each country
		for($x = 0; $x < count($this->locations); $x++) {
			
			$loc = $this->locations[$x];
			
			Debug::show('location ' . (string)$loc); 
			echo 'location ' . (string)$loc;

			//creat the url need for the API request for pickup and drop off data
			$url = $this->thlAPIRequest_url.$loc;

			// Make a curl request
			$params = array('url' => $url,
			         'host' => '',
			        'header' => '',
			        'method' => '',
			        'referer' => '',
			        'cookie' => '',
			        'post_fields' => '',
			        'timeout' => 0
			        );
			$data = $this->_doCurl($params);	
			
		
			//loop through each branch and day for the integrated countries
			for($loc = 0; $loc < count($data); $loc++) {

				$branch = $data[$loc]->branch;
				$code = $branch->code;

			   // Debug::show('branch ' . (string)$branch);
			   // Debug::show('code ' . (string)$code);
			   echo 'branch ...';

				// Bad branch from THL API - these branches are not represented in Deputy just skip them
				if (($code == "BME") || ($code == "HBT")) {
					continue;
				}

				// find correct branch code								
				if(isset($this->unitTranslation[strtoupper($code)])) {
					$code = $this->unitTranslation[strtoupper($code)];
				}
				// get operational unit used for submitting sales data back to deputy
				$unit = $this->_getOperationalUnit($code);

				if(!$unit) {
					Debug::show("could not find correct operational unit. ".$code."\n", TRUE);					
					exit;
				}

				// This function is used to distribute the pickups which come back as a single number from aurora  and distributes them across a day
				$dates = $branch->dates;
				$salesEntry = array();
				for($d = 0; $d < count($dates); $d++) {					
					$salesEntry = array_merge($salesEntry, $this->_distribute($dates[$d]));
				
				}
				
				// limit the update of data to only 3 weeks into the future
				// 9 work hours in a day * 7 days a week * 3 weeks
				$limit = 9 * 7 * 3;				
				$count = 0;


				// loop through each sales entry created and make a post to deputy
				foreach($salesEntry as $ref => $entry) {
					
					if($entry['count'] > 0) {
						// Overriding time zone because Deputy does not handle it correctly
						$timeZone = 'Australia/Sydney';

						date_default_timezone_set($timeZone);
								
						preg_match('/([0-9]+)\/([0-9]+)\/([0-9]+) ([0-9]+:00:00)/', $entry['time'], $m);
											
						$d = $m[3]."-".$m[2]."-".$m[1]." ".$m[4];					
						$date = new DateTime($d);
						
						$timestamp = $date->getTimestamp();
						
						// Adjust time stamp for bug in Deputy
						// Graph in deputy showing 2 hours ahead of where it should be.

						$timestamp = $timestamp - (60 * 60 * 2);
						$tz_string = $date->getTimezone()->getName();
						// make post request
						$post = array('Timestamp'=>$timestamp, 'SalesQty' => $entry['count'], 'SalesRef'=>$code."_".$ref, 'OperationalUnit' => $unit);
						$result = $this->_deputyAPI('resource/SalesData/', 'PUT', $post);

						Debug::show($post);

						if ($count >= $limit) {						
							break(1);
						}					
						$count++;
					}
				}
			}
		}

		$newRelic->createEvent(json_encode(array('eventType'=>'scriptExecute',
							 'scriptName'=>'rentalsdeputy.php',
							 'scriptEnd'=>1)));
	}

	/*
		distribute the single number across the day
	*/
	protected function _distribute($dateObj) {

		$date = $dateObj->date;		
		$pickups = $dateObj->pickup;
		$dropoffs = $dateObj->dropoff;
		$total = array();		
		$pickup_peak =  array(0.1,0.2,0.4,0.2,0.1,0.0,0.0,0.0,0.0);
		$dropoff_peak = array(0.0,0.0,0.0,0.1,0.1,0.2,0.3,0.2,0.1);

		for($s = 0; $s < count($pickup_peak); $s++ ) {

			//$total[$date."_".$s]['count'] = ($pickups * $pickup_peak[$s]) + ($dropoffs * $dropoff_peak[$s]);
			$total[$date."_".$s]['count'] = ($pickups * $pickup_peak[$s]);
			$total[$date."_".$s]['time'] = $date." ".($s+8).":00:00"; 			
		}		
		return $total;

	}

	/*
		used to get the operational unit data for each branch but only try to match the customer service units
	*/

	protected function _getOperationalUnit($code) {

		for($x = 0; $x < count($this->OperationalUnit); $x++) {
			$unit = $this->OperationalUnit[$x];
			if(($unit->CompanyCode == strtoupper($code)) && (preg_match('/customer/i',$unit->OperationalUnitName))) {

				return $unit->Id;
			}
		}
		return false;

	}

	/*
		simple function to put together the deputy API call
	*/

	protected function _deputyAPI($resource, $method = 'GET', $post='') {
		
		$url = "https://" . $this->deputy_endpoint . "/api/v1/" . $resource;
		$params = array('url' => $url,
				        'header' => array(
		              					'Content-type: application/json'
		            					, 'Accept: application/json'
		            					, 'Authorization : OAuth ' . $this->deputy_token
		            					, 'dp-meta-option : none'
		            				),
				        'method' => '',
				        'referer' => '',
				        'cookie' => '',
				        'post_fields' => json_encode($post),
				        'timeout' => 0,
				        'method' => $method
				        );

		return $this->_doCurl($params);

	}

	/*
		Curl request function for all API calls 
	*/
	protected function _doCurl($params) {
        
        $curl = new Curl\CurlRequest();
        try
                {            
                    $curl->init($params);
                    $result = $curl->exec();
                    
                    if ($result['http_code'] > 204) {
                        Debug::show($params);
                        Debug::show($result);
                        
                    }
                    if (!$result['body'])  {
                        Debug::show($params);
                        Debug::show($result);
                                                
                    }

                    if ($result['curl_error'])    throw new Exception($result['curl_error']);
                    if ($result['http_code'] > 204)    throw new Exception("HTTP Code = ".$result['http_code']);
                    if (!$result['body'])        throw new Exception("Body of file is empty");
                    

                }
        catch (Exception $e)
                {
                    echo $e->getMessage();
                }


        return json_decode($result['body']);   

    }

}

// execute
$update = new RentalsDeputy();
